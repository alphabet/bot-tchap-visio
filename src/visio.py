import os 
import re 
import time
import trad
import random
import hashlib
import asyncio
import requests 
import config
from hashlib import sha1
from urllib.parse import urlencode
import xml.etree.ElementTree as ET
import simplematrixbotlib as botlib
from bigbluebutton_api_python import BigBlueButton


language = config.language  

bot_name = config.bot_name
bot_password = config.bot_password
homeserver = config.homeserver
bbb_url = config.bbb_url
bbb_secret = config.bbb_secret

success_msg = trad.translations[language]['success_msg']
need_admin_msg = trad.translations[language]['need_admin_msg']
failed_to_set_description = trad.translations[config.language]['failed_to_set_description']
failed_to_generate_link = trad.translations[language]['failed_to_generate_link']


creds = botlib.Creds(
    homeserver,
    bot_name,
    bot_password,
)


PREFIX = "!" 

creds = botlib.Creds(homeserver, bot_name, bot_password)
bot = botlib.Bot(creds)

def get_matrix_session_token():
    login_url = f"{homeserver}/_matrix/client/r0/login"
    data = {
        "type": "m.login.password",
        "identifier": {"type": "m.id.user", "user": bot_name},
        "password": bot_password,
    }

    response = requests.post(login_url, json=data)
    response.raise_for_status()
    return response.json().get("access_token")


def get_room_names(room_id, session_token):
    url = f"{bot_homeserver}/_matrix/client/v3/rooms/{room_id}/aliases"
    headers = {"Authorization": f"Bearer {session_token}", "Accept": "application/json"}

    try:
        response = requests.get(url, headers=headers)
        response.raise_for_status()
        data = response.json()
        aliases = data.get("aliases", [])
        return aliases[0] if aliases else None
    except requests.exceptions.RequestException as e:
        print(f"Error fetching room aliases for {room_id}: {e}")
        return None


def set_room_description(session_token, room_id, description):
    url = f"{homeserver}/_matrix/client/r0/rooms/{room_id}/state/m.room.topic"
    headers = {
        'Authorization': f'Bearer {session_token}',
        'Content-Type': 'application/json'
    }
    data = {'topic': description}

    response = requests.put(url, json=data, headers=headers)
    if response.status_code == 403:
        error_response = response.json()
        if error_response.get("errcode") == "M_FORBIDDEN" and error_response.get("error") == "You don't have permission to post that to the room. user_level (0) < send_level (100)":
            return (False , need_admin_msg)
    elif response.status_code != 200:
        print(f"Failed to set room description. Status Code: {response.status_code}. Response: {response.text}")
        failure_msg= translations['en']['failed_to_set_description'].format(status_code=response.status_code, response_text=response.text)
        return (False,failure_msg)
    return (True, success_msg)


# Dictionary to store the conference links for each room
conference_links = {}

def create_conference_link(meeting_id, meeting_name):
    print(f"Creating conference link for meeting_id: {meeting_id}, meeting_name: {meeting_name}") # Debug print

    # Parameters for meeting creation
    create_params = {
        'allowStartStopRecording': 'true',
        'autoStartRecording': 'false',
        'meetingID': meeting_id,
        'moderatorPW': 'moderator',
        'name': meeting_name,
        'record': 'false',
    }
    create_params_encoded = urlencode(create_params)

    # Calculate the checksum for creating the meeting
    create_checksum_string = f"create{create_params_encoded}{bbb_secret}"
    create_checksum = sha1(create_checksum_string.encode()).hexdigest()

    # Create the meeting creation URL
    create_url = f"{bbb_url}/create?{create_params_encoded}&checksum={create_checksum}"

    # Create the meeting by making a GET request to the create_url
    create_response = requests.get(create_url)

    # Parse the XML response to check the return code
    response_xml = ET.fromstring(create_response.text)
    returncode = response_xml.find("returncode").text
    message_key = response_xml.find("messageKey").text if response_xml.find("messageKey") is not None else None

    # Check creation response or running status
    if returncode in ['SUCCESS', 'duplicateWarning'] or message_key == 'idNotUnique':
        # Generate a join URL for everyone as a moderator
        join_params = f"fullName=Moderator&meetingID={meeting_id}&password=moderator&redirect=true"
        checksum_string = f"join{join_params}{bbb_secret}"
        checksum = sha1(checksum_string.encode()).hexdigest()
        join_url = f"{bbb_url}/join?{join_params}&checksum={checksum}"

        print("Moderator join URL is :", join_url)
        conference_links[meeting_id] = join_url
        return join_url
    else:
        print(f"Failed to create meeting for room ID: {meeting_id}, meeting name: {meeting_name}")
        return None


@bot.listener.on_message_event
async def handle_message(room, message):
    session_token = get_matrix_session_token()
    room_name = room.display_name
    conference_link = None

    match = botlib.MessageMatch(room, message, bot, PREFIX)
    if match.is_not_from_this_bot() and match.prefix():
        content = message.body.strip()

        # Check if there's already a conference link for this room
        if room.room_id in conference_links:
            conference_link = conference_links[room.room_id]
        else:
            conference_link = create_conference_link(room.room_id, room_name)

        welcome_msg = trad.translations[language]['welcome_msg'].format(conference_link=conference_link)
        description = trad.translations[language]['Description'].format(conference_link=conference_link, room_name=room_name)

        # Check for the specific 'visio' command
        if content == f"{PREFIX}visio":
            if conference_link is not None:
                await bot.api.send_text_message(room.room_id, welcome_msg)
                print(f"Responded to 'visio' command in room {room.room_id}")
            else:
                await bot.api.send_text_message(room.room_id, failed_to_generate_link)

        # Check for the specific 'desc' command
        elif content == f"{PREFIX}desc":
            success, message = set_room_description(session_token, room.room_id, description)

            await bot.api.send_text_message(room.room_id, message)
            print(f"Responded to 'desc' command in room {room.room_id}")

bot.run()


#Automatically delete session.txt and store files
if __name__ == "__main__":
    os.system("rm session.txt | rm -rf store")

    loop = asyncio.get_event_loop()
    loop.run_until_complete(bot.main())
    loop.close()

